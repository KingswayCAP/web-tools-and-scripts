#!/usr/bin/Rscript
suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(argparser, quietly = TRUE))

main <- function() {
  p <- arg_parser("Merge industry details with contact files")
  p <- p %>% add_argument("excel", nargs=1, help = "excel file with Industry, Sub-industry, and Industry Pitch")
  p <- p %>% add_argument("contacts", nargs = 1, help = "contacts cvs")
  argv = parse_args(p)
  
  descriptions <- read_excel(argv$excel) %>% 
    as_tibble() %>% 
    select(`ZoomInfo Company ID`,`Industry`, `Sub-industry`, `Industry Pitch`)
  
  contact_info <- read_csv(argv$contacts, col_types=cols()) %>% 
    select(!c(`Primary Industry`))
  
  updated_info <- contact_info %>% 
    left_join(descriptions, by=c("ZoomInfo Company ID"="ZoomInfo Company ID")) %>%
    replace_na(list("Industry" = "NO INDUSTRY")) %>% 
    filter(Industry != "Revisit")
  
  write_csv(updated_info, argv$contacts,na = "")
}

if(!interactive()){
  main()
}
