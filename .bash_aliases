alias count='find . -type f | wc -l'
alias left='ls -t -1'
alias gh='history|grep'
alias mnt="mount | awk -F' ' '{ printf \"%s\t%s\n\",\$1,\$3; }' | column -t | egrep ^/dev/ | sort"
alias rm="rm -i"
