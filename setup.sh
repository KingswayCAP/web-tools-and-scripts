#!/bin/bash

# SET UP UBUNTU ENVIRONMENT
## Install all the necessary Ubuntu Debian Packages
chmod +x ${HOME}/web-tools-and-scripts/install_apt_packages.sh
bash ${HOME}/web-tools-and-scripts/install_apt_packages.sh

## Copy .bashrc, .bash_aliaes, and .vimrc files
cp ${HOME}/web-tools-and-scripts/.bashrc ${HOME}
cp ${HOME}/web-tools-and-scripts/.bash_aliases ${HOME}
cp ${HOME}/web-tools-and-scripts/.vimrc ${HOME}

## Add Windows Home to PATH 
WINHOME=$(wslpath "$(wslvar USERPROFILE)" | sed 's/\r$//')
echo "export WINHOME=${WINHOME}" >> ${HOME}/.bashrc

## Create users bin folder for symlinks to scripts
mkdir ${HOME}/bin

## Create recycling bin
mkdir ${HOME}/.recycling_bin

## Move bash functions to newly created bin folder
cp ${HOME}/web-tools-and-scripts/del ${HOME}/bin
cp ${HOME}/web-tools-and-scripts/excel ${HOME}/bin
cp ${HOME}/web-tools-and-scripts/open ${HOME}/bin

## Create symlink to Windows home folder
ln -s $WINHOME ${HOME}/windows 

## Install Vundle for vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
### Install plugins to vim
vim +PluginInstall +qall

# SET UP R ENVIRONMENT
## Add the correct repositories for R and delete old R libraries
bash ${HOME}/web-tools-and-scripts/add_r_libraries.R

## Install all the necessary R packages
chmod +x ${HOME}/web-tools-and-scripts/install_r_packages.R
sudo Rscript ${HOME}/web-tools-and-scripts/install_r_packages.R

## Set up .Renviron to avoid annoying timedatectl error
TZ=$(cat /etc/timezone)
echo "TZ = \"${TZ}\"" > ${HOME}/.Renviron

## Create folders needed by R scripts
mkdir ${WINHOME}/zoominfo
mkdir ${WINHOME}/zoominfo/ready_for_upload/
touch ${WINHOME}/zoominfo/ready_for_upload/.exported_companies.csv
mkdir ${WINHOME}/zoominfo/.sfdc_results/

## Create symlinks for R scripts 
ln -s ~/web-tools-and-scripts/clean_zoom_info.R ~/bin/clean_zoominfo
ln -s ~/web-tools-and-scripts/scrape_zoom_info.R ~/bin/scrape_zoominfo
ln -s ~/web-tools-and-scripts/send_to_salesforce.R ~/bin/upload_leads_to_salesforce
ln -s ~/web-tools-and-scripts/make_tags.R ~/bin/make_tags
ln -s ~/web-tools-and-scripts/scrape_capterra.r ~/bin/download_capterra
ln -s ~/web-tools-and-scripts/scrape_clutch.R ~/bin/scrape_clutch
ln -s ~/web-tools-and-scripts/scrape_gregslist.R ~/bin/download_gregslist
ln -s ~/web-tools-and-scripts/scrape_staffmarket.R ~/bin/download_staffmarket
ln -s ~/web-tools-and-scripts/merge_details.R ~/bin/merge_details

## Unlink the config files from git
cd ~/web-tools-and-scripts/
git update-index --skip-worktree ~/web-tools-and-scripts/geography.R
git update-index --skip-worktree ~/web-tools-and-scripts/users.R
git update-index --skip-worktree ~/web-tools-and-scripts/pitches.R
git update-index --skip-worktree ~/web-tools-and-scripts/campaigns.R
