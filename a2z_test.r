foo <- read_csv("~/dokah/zoom_info/a2z_it.csv")
foo %>% filter(!(`Business Name` %in% pull(bar, Company))) %>% 
  select(!c(Source, Date)) %>% 
  filter(!(`Business Name` %in% pull(bar, ZI))) %>%
  filter(!(`Business Name` %in% pull(expt, Company))) %>% 
  filter(`Year Established` <= 2005) %>% 
  relocate(c(`Revenue / Yr`, `Location Employee Size`), .before=`Physical Address`) %>% 
  write_csv("~/dokah/zoom_info/msp/ca_a2z_msp.csv")