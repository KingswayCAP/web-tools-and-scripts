#!/usr/bin/Rscript

suppressMessages(library(httr))
suppressMessages(library(jsonlite))
suppressMessages(library(rvest))
suppressMessages(library(argparser, quietly = TRUE))
suppressMessages(library(tidyverse))

hush=function(code){
  sink("/dev/null") # use /dev/null in UNIX
  tmp = code
  sink()
  return(tmp)
}

download_capterra <- function(output_folder) {
  links <- read_html("https://www.capterra.com/categories") 
  
  temp <- links %>% 
    html_nodes(".portable-one-whole a") %>% 
    html_attr("href") %>% 
    str_match("\\/(.*)\\/")
  
  temp <- temp[, 2]
  
  temp2 <- na.omit(temp)
  
  map(temp2, function(x){
    scrape_capterra(x) %>% 
      write_csv(paste0(output_folder,x, ".csv"))
  })
}

scrape_capterra <- function(category) {
  url <- paste0("https://www.capterra.com/directoryPage/rest/v1/category?htmlName=", category,"&rbr=0&countryCode=US")
  data <- httr::GET(url=url) %>% 
    content()
  
  products <- data$pageData$categoryData$products
  lapply(products, parse_product) %>% 
    bind_rows()
}

parse_product <- function(product) {
  cols <- c('product_name', 'vendor', 'short_desc', 'long_desc','url')
  info <- rep("", length(cols))
  names(info) <- cols
  try(info['product_name'] <- product$product_name)
  try(info['vendor'] <- product$vendor$vendor_name)
  try(info['short_desc'] <- product$short_desc)
  try(info['long_desc'] <- product$long_desc)
  try(info['url'] <- product$product_url)
  info %>% t %>% as_tibble
}

create_compilation <- function(categories=c("field-service-management"), output_file="/dev/null") {
  files = list.files("~/dokah/capterra/", pattern="*.csv")
  files <- files[files %in% map(categories, function(x) paste0(x, "-software.csv")) %>%  as_vector()]
  setwd("~/dokah/capterra/")
  lapply(files, read_csv) %>% 
    reduce(bind_rows) %>% 
    distinct() %>% 
    write_csv(output_file)
}

main <- function() {
  p <- arg_parser("Download part or all of Capterra.com")
  p <- p %>% add_argument("output", nargs = 1, help="If a category is provided this should be output file, otherwise this will be the output folder")
  p <- p %>% add_argument("--category", nargs = 1, help = "If an individual category is provided, script will download singular category to file. List of categories available here: www.capterra.com/categories, e.g. ab-testing-software for AB Testing Software")
  p <- p %>% add_argument("--debug", flag=TRUE, help = "allow printing of error messages")
  argv = parse_args(p)
  
  if(!argv$debug){
    dev_null <- file("/dev/null", open = "wt")
    sink(dev_null, type=c("message"))
  }
  
  if(is.na(argv$category)) {
    download_capterra(argv$output)
  } else {
    data <- scrape_capterra(argv$category) %>% 
      write_csv(argv$output)
  }
  quit(status=0)
}

if (!interactive()) {
  main()
}
