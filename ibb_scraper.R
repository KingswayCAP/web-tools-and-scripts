library(tidyverse)
library(rvest)
library(hash)
library(stringr)
library(V8)

get_brokers_for_state <- function(state) {
  site_url <- str_to_lower(paste0("https://www.ibba.org/state/",state))
  page <- read_html(site_url)
  existing <- read_csv("~/dokah/brokers/existing_brokers.csv")
  emails <- existing %>% pull(Email)
  
  broker_urls <- page %>% 
    html_nodes(".brokers__item--link") %>% 
    html_attr("href")
  
  output_tibble <- broker_urls %>% 
    lapply(process_broker_urls) %>% 
    reduce(bind_rows) %>% 
    filter(!(`email` %in% emails)) %>% 
    write_csv(paste0("~/dokah/brokers/", state, ".csv"))
}

process_broker_urls <- function(broker_url) {
  cols <- c("name", "email", "phone", "company", "street", "city", "state", "zip")
  data <- rep("", length(cols))
  names(data) <- cols
  
  writeLines(
    sprintf(
      "var page = require('webpage').create();
    page.open('%s', function () {
      console.log(page.content); //page source
      phantom.exit();
    });", broker_url), con="/tmp/scraper/scrape.js")
  
  system("phantomjs /tmp/scraper/scrape.js > /tmp/scraper/scrape.html", intern = T)
  
  broker_page <- read_html("/tmp/scraper/scrape.html")
  data['name'] <-  
    broker_page %>%
    html_nodes(".brokers__profile--informationName") %>%
    html_text() %>% 
    str_to_title() %>% 
    str_trim()
  
  phone_email <-  
    broker_page %>%
    html_nodes(".brokers__profile--leftPhone a") %>%
    html_text() %>% 
    str_trim()
  
  
  if (length(phone_email) == 2) {
    try(data['phone'] <- phone_email[1])
    try(data['email'] <- phone_email[2])
  } else {
    try(data['email'] <- phone_email[1])
  }
  
  company <- 
    broker_page %>% 
    html_nodes(".brokers__profile--leftAddress span") %>% 
    html_text() %>% 
    str_trim()
  
  try(data['company'] <- company)
  
  address <- 
    broker_page %>% 
    html_nodes(".brokers__profile--leftCityLocation") %>% 
    html_text() %>% 
    str_trim() %>% 
    paste(collapse=" ")
  
  try(data['street'] <- address)
  
  city_state_zip <- broker_page %>% 
    html_nodes(".brokers__profile--leftCityBottom") %>% 
    html_text() %>% 
    str_trim() %>% 
    str_match("(^.*),\\s(\\w+)\\s(\\d+)")
  
  try(data['city'] <- city_state_zip[2])
  try(data['state'] <- city_state_zip[3])
  try(data['zip'] <- city_state_zip[4])
  
  as_tibble(t(data))
}

run_all_states <- function(removed=c('AZ', 'CA', 'NM', 'NV', 'OR', 'WA', 'TX', 'GA')){
  states <- read_csv("~/dokah/brokers/states.csv") %>% 
    filter(!(`Abbreviation` %in% removed)) %>% 
    select(`State`) %>% 
    mutate(`State` = str_replace_all(`State`, ' ', '-')) %>% 
    pull(`State`) %>% 
    map(get_brokers_for_state)
}
