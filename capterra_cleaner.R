#!/usr/bin/Rscript

library(tidyverse)
library(argparser)

main <- function() {
  p <- arg_parser("Clean URLs for upload to ZoomInfo")
  p <- p %>% 
    add_argument("filename", nargs=1, help = "Capterra file csv name e.g. hvac-software.csv")
  
  argv = parse_args(p)
  setwd("~/dokah/capterra")
  
  foo <- read_csv(argv$filename) %>% 
    mutate('url' = str_replace(`url`, 'https', 'http')) %>% 
    mutate('url' = str_match(`url`,"http\\:\\/\\/([\\w\\.\\-\\_]*\\.\\w*)\\/??")[,2]) %>%  
    select(`vendor`, `url`) %>% 
    drop_na('url') %>% 
    rename('Company' = `vendor`) %>% 
    distinct()
  
  setwd("~/dokah/capterra/temp")
  foo %>% write_csv(argv$filename)
}

if(!interactive()) {
  main()
}