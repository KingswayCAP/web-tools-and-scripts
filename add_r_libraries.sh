#!/bin/bash

sudo apt -y remove r-base

# delete all the old R 3.xx library
sudo rm -rf /usr/local/lib/R/site-library/*

# update indices
sudo apt -y update -qq
# install two helper packages we need
sudo apt -y install --no-install-recommends software-properties-common dirmngr

# install some dependencies
sudo apt-get install dirmngr gnupg apt-transport-https ca-certificates software-properties-common

# import the signing key (by Michael Rutter) for these repo
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"

sudo apt -y update
sudo apt -y dist-upgrade
sudo apt -y install r-base r-base-core r-recommended
