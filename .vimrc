set nocompatible
filetype off

"execute pathogen#infect()
"call pathogen#helptags()

" Packages
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fireplace'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-projectionist'
Plugin 'tpope/vim-dispatch'
Plugin 'tpope/vim-salve'
Plugin 'tpope/vim-surround'
Plugin 'vim-scripts/paredit.vim'
Plugin 'guns/vim-clojure-static'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'bling/vim-airline'
Plugin 'venantius/vim-eastwood'
Plugin 'venantius/vim-cljfmt'
Plugin 'flazz/vim-colorschemes'
call vundle#end()


syntax enable
filetype plugin indent on
autocmd BufNewFile,BufReadPost *.cljx setfiletype clojure
let g:clojure_fuzzy_indent_patterns = ['^with', '^def', '^let', '^facts', '^fact', '^context', '^table', '^subselect']

"Turn on nerdtree

colorscheme vividchalk 
"colorscheme molokai 
"colorscheme candy
"colorscheme vibrantink
"colorscheme autumn


"line numbering
set number 
set relativenumber

"find out where cursor is
set ruler 
set hidden
set autoread

"good tab settings
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set autoindent

"smart search, ignores case if search term is lower case
set ignorecase
set smartcase
imap jk <Esc>

let g:rbpt_max = 16
let g:rbpt_colorpairs = [
    \ ['gray',        'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['yellow',       'RoyalBlue3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]
let g:rbpt_loadcmd_toggle = 0
syntax on

let mapleader=","
"au VimEnter * RainbowParenthesesToggle


if has("gui_running")
    if has("gui_macvim")
        set guifont=Monaco:h13
    endif
endif

"autocmd VimEnter * NERDTree

"clojure indent strings to line up
let g:clojure_align_multiline_strings = 1

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 2
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"au Syntax * RainbowParenthesesLoadRound
"au Syntax * RainbowParenthesesLoadSquare
"au Syntax * RainbowParenthesesLoadBraces

nnoremap <Leader>a :NERDTreeToggle<CR>
nnoremap <Leader>f :NERDTreeFocus<CR>
nnoremap <Leader><Space> :Goyo<CR>  
nnoremap <Leader>wk <C-w>k
nnoremap <Leader>wj <C-w>j
nnoremap <Leader>wh <C-w>h
nnoremap <Leader>wl <C-w>l
