#!/bin/bash 

sudo apt -y update
sudo apt -y upgrade
sudo apt -y autoremove
sudo apt -y install pdftk
sudo apt -y remove gpg
sudo apt -y install gnupg1
sudo apt -y install r-base
sudo apt -y install gdebi-core
sudo apt -y install texlive-latex-base
sudo apt -y install texlive-fonts-recommended
sudo apt -y install libssl-dev
sudo apt -y install libssl-doc
sudo apt -y install libxml2-dev
sudo apt -y install libcurl4-openssl-dev
sudo apt -y install libv8-dev
sudo apt -y install phantomjs
sudo apt -y install pacman
sudo apt -y install vim-gtk3
sudo apt -y install xcb
sudo apt -y install software-properties-common
sudo apt -y install firefox
sudo apt -y install nvidia-common
sudo apt -y install nvidia-driver-460
sudo apt -y install netsurf
sudo apt -y install ubuntu-desktop
sudo apt -y install pdfgrep
sudo apt -y install rename
sudo apt -y install diffpdf
sudo apt -y install wget
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.4.1106-amd64.deb
sudo gdebi rstudio-server-1.4.1106-amd64.deb
sudo rm rstudio-server-1.4.1106-amd64.deb

